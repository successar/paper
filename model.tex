\section{Preprocessing KB and Questions}
\subsection{Processing FREEBASE}
\textbf{Mediators : }First, a freebase triple may also contain an object entity as mediator node or CVT (compound value type), which is tuple of related values (of different types). That is, such a fact is of type $(s, R, ((R'_1, o_1), \dots, (R'_n, o_n))$ where $(R'_i, o_i)$ is a relationship-entity pair, creating an 2 Hop path between subject and object. For example, this type of node is used to associate a time-period with a value (eg Population of a country) over which it is valid. We remove this compound fact from KB and add all facts of form $(s, R'_i, o_i)$, keeping only the second relationship. As part of its context, each such fact can also refer to its \textbf{sibling facts} $(s, R'_j, o_j)_{j \ne i}$ if needed.
\par
\textbf{Grouping : }Secondly, We redefine a fact as being a triple containing a subject entity, a relationship, and the list of all object entities (of same type), called \textbf{answer group}, linked to the same subject by the same relationship. This grouping process transforms atomic facts into grouped facts, which we simply refer to as facts in the subsequent sections. Such a grouping gives significant boost in efficiency when answering list type questions as well as increase the breadth of search space around candidate entities by reducing number of facts from 22M to 12M on FB5M.
\par
\textbf{Fact Representation : }Each entity in KB is represented as a d-dimensional vector $e_s \in \mathbb{R}^d_\mathcal{E}$. Similarly, each relationship is represented as d-dimensional vector $e_r$ in a separate embedding space $\mathbb{R}^d_\mathcal{R}$.
\par
 We initialize entity/relationship space by computing an entity/relationship vector as the average of its word vectors (over alias that appears in the question in case of entities), where each word vector is of dimension $d$. Such a scheme has the additional advantage that we can benefit from pre-trained unsupervised word vectors (eg from Word2Vec), which in general capture some distributional syntactic and semantic information.
 \par
 Therefore, we represent each fact as $f = (s, R, O^{(m)}) \in R^d_\mathcal{E} \times R^d_\mathcal{R} \times R_\mathcal{E}^{d \times |O^{(m)}|}$  where $O^{(m)}$ is the grouped list of objects (indexed by $m$) related to entity $s$ by $R$. For each object $o^{(i)} \in O^{(m)}$, the fact $f' = (s, R, \{o^{(i)}\})$ is called a \textbf{sub-fact}.

\subsection{Processing Reverb KB}
We use precomputed entity links \cite{lin2012entity} to link the subject and the object of a Reverb fact to Freebase entities. If such links do not give any result for an entity, we  search for Freebase entities with at least one alias that matches the Reverb entity string. These two processes allowed to match 17\% of Reverb entities to Freebase ones. The remainder of entities were encoded using similar method as Freebase entities.
It should be noted that in our experiments, facts in this KB were not included during training of our system, but added during the evaluation on Reverb Questions. 

\subsection{Processing Question}

Let $q$ be the question (sequence of words $x_1, x_2, \dots, x_{N_q}$). We experimented with 2 different ways to represent it:
\begin{itemize}[leftmargin=*]
	\item \textbf{Bag-of-words (BOW) : } It is sum of embeddings of individual word vectors ie $q = \sum_{i} e_{x_i}$.
	\item \textbf{Position Encoding (PE) :} This encoding take in account order of words in the question (initial part of summed vector is weighed higher for initial words and vice versa). The embedding is of form $e_q = \sum_{i} l_i \odot e_{x_i}$ where $\odot$ is an element-wise multiplication. $l_i$ is a column vector with the structure $l^j_i = \min(\frac{i.d}{j.N_q}, \frac{j.N_q}{i.d})$ (assuming 1-based indexing), where d is the dimension of the embedding. 
\end{itemize}

\section{Model Description}
\subsection{Fact Extraction}
\label{factgen}
To begin, we generate initial list of facts (called candidate facts) over which reasoning will start. We define a hyper-parameter cutoff value $N_f$ that is the maximum number of candidate facts that can be stored for forward computation at any instant of time. To generate candidate facts, we use following heuristic: 
Following \cite{bordes2015large}, we generate all possible n-grams from the question, removing those that contain an interrogative pronoun or 1-grams that belong to a list of stopwords. We only keep the n-grams which are an alias of an entity, and then discard all n-grams that are a subsequence of another n-gram, except if the longer n-gram only differs by \textit{in}, \textit{of}, \textit{for} or \textit{the} at the beginning. 
\par 
For each entity identified (and its n-gram), we extract all its facts and assign them a score as len(n-gram) + degree(entity) + $q^T Q(e_s, e_R)$ ($Q$-function is defined in next section). We add the highest scoring facts such that total number of candidate facts should not exceed $N_f$.

\subsection{Factual Memory Network}
At the end of preprocessing, we have a list of initial candidate facts $\mathcal{F} = \{f_k\}$ of form $(s_k, R_k, O^{(m)}_k)$ with each member of tuple in its embedded format as described above and a question embedding $q \in \mathbb{R}^d$. We now define a mechanism to perform reasoning over our candidate facts. 
\subsubsection{Single Layer}
A single layer of our model takes in a question $q$ and list of candidate facts ($|\mathcal{F}| \le N_f$) and returns scores for them as described below.
\begin{equation}
g_k = q^T Q - |A(Q, O^{(m)}_k) - A(q, O^{(m)}_k)|
\label{eq:gk}
\end{equation}
\begin{equation}
Q = W_C[e_s;e_R], A(q, O) = q^T \frac{1}{|O|} \sum_{m} e_{o^{(m)}}
\label{eq:QA}
\end{equation}
\begin{equation}
h_k = \frac{exp(g_k)}{\sum_k exp(g_k)}
\end{equation}
We visualize a fact $(s, R, O^{(m)})$ as a question-answer pair with $(s, R)$ forming the question and $O^{(m)}$ answer. Function $Q$ (``Question") maps subject and relationship to single vector in $\mathbb{R}^d$. The goal of this is to calculate the ``question" like representation of given fact. 
\par Function $A$ (``Answer") computes a high scalar value if the given ``question" is related to the given ``answer". Naturally, it should give high score when computing $A(Q, O^{(m)})$ for a fact in KB. This is satisfied by pre-training them on complete Freebase KB as given in \cite{socher2013reasoning}. We also expect that it should high score if given question is related to answer entities in some form and consequently low value for second term in Eq.\eqref{eq:gk}. This happen during minimization of ranking and squared loss in next section.
\par
The final score for a fact $f_k$ is calculated using softmax function over all facts to determine their relative importance.

\begin{figure}
	\includegraphics[width=\columnwidth]{slayer.pdf}
	\caption{Recursive Structure of our model.}
	\label{fig:mod}
\end{figure}

\subsubsection{Multiple Layers}
We now extend out model to L layers/hops. The layers are stacked recursively (Fig. \ref{fig:mod}) as follows:
\begin{enumerate}[leftmargin=*]
	\item \textbf{Fact Pruning : } We prune the candidate fact list before entering them in next layer. This can be performed by choosing a pruning threshold $\kappa$ and removing facts with $h_k < \kappa$. We found in our experiments that $\kappa = \frac{\max_k(h_k)}{2}$ gives best results. Performing pruning seems to remove unnecessary facts from subsequent computation, significantly improve our response time and allows us to explore larger search space around the subgraphs of our question entities.
	\par
	\textbf{Answer Pruning : } Another type of pruning occurs within a fact $f_k$ when its \textbf{sub-facts} were generated from CVT. Then for each sub-fact $m$, we extract all its \textbf{sibling facts}, calculate Eq.\eqref{eq:gk} for each and average the results to get $g_m$, then calculate $h_m$ for the sub-fact using softmax over \textbf{answer group} $O^{(m)}$. We eliminate those answers from answer group whose $h_m < \frac{\max_m(h_m)}{2}$.
	\item The question embedding for subsequent layer is calculated as follows :
	\begin{equation}
	q_{n+1} = q_n + \sum_{f_k} h_k Q(e_s,e_R)
	\end{equation}
	\item \textbf{Fact Addition : } We include new facts by extending the subgraphs of entities explored. For each object entity $o$ belonging to some $O^{(m)}_k$, we find all new facts (not present in candidate list) $(o, R'', o'')$ and assign a score $h_k q_{n+1}^T Q(e_o,e_{R''})$ to them. We add highest scoring facts as candidates for next layer such that fact-score $> \kappa$ and total number of candidate facts $|\mathcal{F}| \le N_f$.	
\end{enumerate}

\subsubsection{Output Function}
To generate answer at the topmost layer L, We calculate $h_k$ and perform Fact and Answer Pruning on the candidate fact list. For each remaining fact $f_k$ in candidate fact list, we calculate the following scoring function:
\begin{equation}
S(q_L, f_k) = g_k A(q_L, O^{(m)}_k)
\label{eq:sc}
\end{equation}
Finally, output of our model is defined as follows :
\begin{equation}
a^* = arg\max_{O^{(m)}_k} S(q, f_k)
\end{equation}

\subsection{Paraphrases Supervision}
Following previous work such as \cite{fader-zettlemoyer-etzioni:2013:ACL2013,bordes2015large}, we also use the question paraphrases dataset WIKIANSWERS to generalize for words and question patterns which are unseen in the training set of question-answer pairs. We minimize hinge loss defined in next section so that a question and its paraphrases should map to similar representations.

\subsection{Training Objectives}
Let the training set be $\mathcal{D} = \{(q_i, a_i)\}$, where $q_i$ is the question with list of correct answer $a_i$ (mapped to corresponding entities). We define following loss variables: 
	\par \textbf{Ranking Loss:} This is to train our entity embeddings such that our scoring function (Eq.\eqref{eq:sc}) gives higher score to correct answer. Since we score over facts rather than answers, we need to generate candidate answer facts over which the score is to be minimized.
	\begin{equation}
	L_{RK} = \sum_{\mathcal{D}} \sum_{f \in \mathcal{F^*}} max\{0, 0.1 - S(q, f) + S(q, \bar{f})\}
	\end{equation}
	The candidate fact list $\mathcal{F^*}$ contains all the facts $f$ such that its $O^{(m)} \subset a_i$ and $f$ occurs along any of two shortest paths between the question entities (generated in section \ref{factgen}) and answer entity in KB. 
	The negative candidate fact $\bar{f}$ is generated by sampling 60\% of time a random fact from KB and 40\% of time from the set of facts containing $a_i$ but not on paths starting from $q_i$. 
	\par \textbf{Squared Loss:} To train our layer parameters, we minimize the following squared loss function of answer embeddings (over all layers):
\begin{equation}
L_{SQ} = \sum_{\mathcal{D}} \sum_{\text{layer n}} \frac{n}{L} \|a_i - \sum_{f_k \in \mathcal{F}_n} (h_k . \frac{1}{|O^{(m)}|} \sum_{O^{(m)}_k} e_o) \|^2
\end{equation}
Here $L$ is number of layers/hops in the network.
This loss function defines the degree to which the object entities in selected facts of a given layer corresponds to answer entity, weighted by $(h_k)$ ie degree to which the question is similar to the subject and relation in the fact. It was observed that minimizing this function gives higher weights to facts in which object entities are similar to answer entities as well as prefer shorter paths to reach the answer from the question.
\par \textbf{Paraphrasing Loss:} For the paraphrase dataset $\mathcal{P} = \{(q, q')\}$ where $q$ and $q'$ are paraphrases, the loss is: 
\begin{equation}
L_{PP} = \sum_{\mathcal{P}} max\{0, 0.1 - q^Tq' + q^Tq''\}
\end{equation}
where $q''$ is random question not belonging to paraphrase cluster of question $q$.
\par
We perform 2 phases of training : in first phase, the Ranking + Paraphrasing loss (without using any layer) is minimized while in second phase Ranking + Squared Loss is minimized (using single or multiple layers). Backpropagation is used to calculate gradients while Adagrad was used to perform optimisation. We used $L_2$ and max-norm regularisation for weight and embedding matrices respectively. The embeddings are constrained to remain on a unit ball in $\mathbb{R}^d$. 